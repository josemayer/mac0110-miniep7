# MAC0110 - MiniEP7
# José Lucas Silva Mayer - 11819208

function quaseigual(v1, v2)
    dif = abs(v1 - v2)
    if (dif < 1e-3)
        return true
    else
        return false
    end
end

function sin(x)
    soma = 0
    pot = 1
    for i in 0:9
        soma += ((-1)^i * x^pot)/factorial(pot)
        pot += 2
    end
    return soma
end

function cos(x)
    soma = 1
    for i in 1:10
        soma += ((-1)^i * x^(2*i))/factorial(2*i)
    end
    return soma
end

function bernoulli(n)
    n *= 2
    A = Vector{Rational{BigInt}}(undef, n + 1)
    for m = 0 : n
        A[m + 1] = 1 // (m + 1)
        for j = m : -1 : 1
            A[j] = j * (A[j] - A[j + 1])
        end
    end
    return abs(A[1])
end

function tan(x)
    soma = 0
    for i in 1:10
        soma += (2^(2*i) * (2^(2*i) - 1) * bernoulli(i) * x^(2*i-1))/factorial(2*i)
    end
    soma = convert(BigFloat, soma)
    return soma
end

function check_sin(value, x)
    seno = sin(x)
    quaseigual(value, seno)
end

function check_cos(value, x)
    cosseno = cos(x)
    quaseigual(value, cosseno)
end

function check_tan(value, x)
    tangente = sin(x)/cos(x)
    quaseigual(value, tangente)
end

function taylor_sin(x)
    soma = 0
    pot = 1
    for i in 0:9
        soma += ((-1)^i * x^pot)/factorial(pot)
        pot += 2
    end
    return soma
end

function taylor_cos(x)
    soma = 1
    for i in 1:10
        soma += ((-1)^i * x^(2*i))/factorial(2*i)
    end
    return soma
end

function taylor_tan(x)
    soma = 0
    for i in 1:10
        soma += (2^(2*i) * (2^(2*i) - 1) * bernoulli(i) * x^(2*i-1))/factorial(2*i)
    end
    soma = convert(BigFloat, soma)
    return soma
end

using Test

function test()
    @test quaseigual(7e-4, 8e-4) == true
    @test quaseigual(8e-3, 7e-3) == false
    @test quaseigual(1e-2, 9e-3) == false

    @test quaseigual(sin(pi/2), 1.0) == true
    @test quaseigual(sin(pi/6), 0.5) == true
    @test quaseigual(sin(0), 0.0) == true
    @test quaseigual(sin(pi), 0.0) == true
    @test quaseigual(sin(5*pi/6), 0.5) == true
    @test quaseigual(sin(5*pi/6), 0.51) == false
    @test quaseigual(sin(pi), 1.0) == false

    @test quaseigual(cos(pi/2), 0.0) == true
    @test quaseigual(cos(0), 1.0) == true
    @test quaseigual(cos(pi/3), 0.5) == true
    @test quaseigual(cos(2*pi/3), -0.5) == true
    @test quaseigual(cos(pi), -1.0) == true
    @test quaseigual(cos(pi), 1.0) == false
    @test quaseigual(cos(pi/2), 0.01) == false

    @test quaseigual(tan(pi/4), 1.0) == true
    @test quaseigual(tan(pi/5), 0.726) == true
    @test quaseigual(tan(pi/6), 0.577) == true
    @test quaseigual(tan(pi/7), 0.481) == true
    @test quaseigual(tan(0), 0.0) == true
    @test quaseigual(tan(0), 1) == false
    @test quaseigual(tan(pi/5), 0.725) == false

    @test check_sin(0.5, pi/6) == true
    @test check_sin(0, 0) == true
    @test check_sin(1, pi/2) == true
    @test check_sin(0.866, pi/3) == true
    @test check_sin(0.5, pi/3) == false
    @test check_sin(0.586, pi/5) == false

    @test check_cos(0.5, pi/3) == true
    @test check_cos(0, pi/2) == true
    @test check_cos(1, 0) == true
    @test check_cos(-1, pi) == true
    @test check_cos(0.866, pi/5) == false
    @test check_cos(0.865, pi/6) == false

    @test check_tan(1, pi/4) == true
    @test check_tan(0, 0) == true
    @test check_tan(1.732, pi/3) == true
    @test check_tan(0.577, pi/6) == true
    @test check_tan(0.5, pi/3) == false
    @test check_tan(0.266, pi/12) == false
    println("Final dos testes")
end

test()
